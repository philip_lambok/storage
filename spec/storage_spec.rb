# frozen_string_literal: true

RSpec.describe Storage do
  describe '#insert' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      expect(storage.count).to eq 1
      record = storage.last
      expect(record).to eq({ name: 'pquest' })
    end

    it 'raise error when params was blank' do
      storage = described_class.new
      expect do
        storage.insert(nil)
      end.to raise_error Storage::InvalidRecord, 'Record was invalid'
    end
  end

  pending '#delete' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      storage.delete({ name: 'pquest' })
      expect(storage.count).to eq 1
      record = storage.last
      expect(record).to eq({ name: 'kokomi' })
    end

    it 'returns expected response when has duplicate column' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      storage.delete({ name: 'pquest' })
      expect(storage.count).to eq 2
    end

    it 'raise error when delete not found record' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect do
        storage.delete({ name: 'not-found' })
      end.to raise_error Storage::CouldNotDeleted, 'Record cannot be deleted'
    end
  end

  pending '#delete_all' do
    it 'returns exected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      storage.delete_all(name: 'pquest')
      expect(storage.count).to eq 1
    end

    it 'clear the storage when params was blank' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      storage.delete_all
      expect(storage.count).to be_zero
    end
  end

  pending '#all' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      records = [
        { name: 'pquest' },
        { name: 'pquest' },
        { name: 'kokomi' }
      ]
      expect(storage.all).to eq(records)
    end
  end

  pending '#include?' do
    it 'returns true when record persisted' do
      storage = described_class.new
      storage.insert({ name: 'andi', balance_in_idr: 100_000 })
      storage.insert({ name: 'pquest', balance_in_idr: 200_000 })
      storage.insert({ name: 'kokomi', balance_in_idr: 500_000 })
      expect(storage.include?(name: 'pquest')).to eq true
    end

    it 'returns false when record blank' do
      storage = described_class.new
      storage.insert({ name: 'andi', balance_in_idr: 100_000 })
      storage.insert({ name: 'pquest', balance_in_idr: 200_000 })
      storage.insert({ name: 'kokomi', balance_in_idr: 500_000 })
      expect(storage.include?(name: 'budi')).to eq false
    end
  end

  pending '#find' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect(storage.find(3)).to eq({ name: 'kokomi' })
    end

    it 'returns nil when record was blank' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect(storage.find(15)).to eq nil
    end

    it 'returns nil when finding 0 index' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect(storage.find(0)).to eq nil
    end
  end

  pending '#find!' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect(storage.find!(3)).to eq({ name: 'kokomi' })
    end

    it 'raise error when record not found' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect do
        storage.find!(10)
      end.to raise_error(Storage::RecordNotFound, 'Record not found')
    end
  end

  pending '#find_by' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect(storage.find_by(name: 'kokomi')).to eq({ name: 'kokomi' })
    end

    it 'returns blank when record not found' do
      storage = described_class.new
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'pquest' })
      storage.insert({ name: 'kokomi' })
      expect(storage.find_by(name: 'invalid-record')).to eq nil
    end
  end

  pending '#where_by' do
    it 'returns expected response' do
      storage = described_class.new
      storage.insert({ name: 'andi', admin: true })
      storage.insert({ name: 'pquest', admin: true })
      storage.insert({ name: 'kokomi', admin: false })
      records = storage.where_by(admin: true)
      expect(records.count).to eq 2
      expect(records).to eq([
                              { name: 'andi', admin: true },
                              { name: 'pquest', admin: true }
                            ])
    end
  end

  pending '#update' do
    it 'returns updated records' do
      storage = described_class.new
      storage.insert({ name: 'andi', admin: true })
      storage.insert({ name: 'pquest', admin: true })
      storage.insert({ name: 'kokomi', admin: false })

      updated_record = { name: 'pquest', admin: false }
      storage.update(2, updated_record)
      records = [
        { name: 'andi', admin: true },
        { name: 'pquest', admin: false },
        { name: 'kokomi', admin: false }
      ]
      expect(storage.all).to eq records
    end

    it 'raise error when try update blank record' do
      storage = described_class.new
      storage.insert({ name: 'andi', admin: true })
      storage.insert({ name: 'pquest', admin: true })
      storage.insert({ name: 'kokomi', admin: false })

      updated_record = { name: 'pquest', admin: false }
      expect do
        storage.update(10, updated_record)
      end.to raise_error Storage::RecordNotFound, 'Record not found'
    end
  end

  pending '#exclude_attribute' do
    it 'returns filtered attribute in mass access' do
      storage = described_class.new
      storage.exclude_attribute(:password)
      storage.insert({ name: 'andi', password: 'secret123' })
      storage.insert({ name: 'pquest', password: 'password123' })
      storage.insert({ name: 'kokomi', password: 'dolphin123' })
      expect(storage.all).to eq([
                                  { name: 'andi' },
                                  { name: 'pquest' },
                                  { name: 'kokomi' }
                                ])
    end
  end

  pending '#sum' do
    it 'returns expected' do
      storage = described_class.new
      storage.insert({ name: 'andi', balance_in_idr: 100_000 })
      storage.insert({ name: 'pquest', balance_in_idr: 200_000 })
      storage.insert({ name: 'kokomi', balance_in_idr: 500_000 })
      expect(storage.sum(:balance_in_idr)).to eq 800_000
    end

    it 'raise error when calculation error' do
      storage = described_class.new
      storage.insert({ name: 'andi', balance_in_idr: 100_000 })
      storage.insert({ name: 'pquest', balance_in_idr: 'invalid-amount' })
      storage.insert({ name: 'kokomi', balance_in_idr: 500_000 })
      expect do
        storage.sum(:balance_in_idr)
      end.to raise_error Storage::CalculationError, 'Calculation error'
    end
  end

  pending '#take' do
    it 'returns expected' do
      storage = described_class.new
      storage.insert({ name: 'andi', balance_in_idr: 100_000 })
      storage.insert({ name: 'pquest', balance_in_idr: 200_000 })
      storage.insert({ name: 'kokomi', balance_in_idr: 500_000 })
      records = [
        { name: 'andi', balance_in_idr: 100_000 },
        { name: 'pquest', balance_in_idr: 200_000 }
      ]
      expect(storage.take(2)).to eq records
    end

    it 'raise error when take more than actual records' do
      storage = described_class.new
      storage.insert({ name: 'andi', balance_in_idr: 100_000 })
      storage.insert({ name: 'pquest', balance_in_idr: 200_000 })
      storage.insert({ name: 'kokomi', balance_in_idr: 500_000 })
      records = [
        { name: 'andi', balance_in_idr: 100_000 },
        { name: 'pquest', balance_in_idr: 200_000 },
        { name: 'kokomi', balance_in_idr: 500_000 }
      ]
      expect(storage.take(10)).to eq records
    end
  end

  pending '#enable_generated_id' do
    it 'returns expected records' do
      storage = described_class.new
      storage.enable_generated_id!
      storage.insert({ name: 'andi' })
      storage.insert({ name: 'pquest' })
      records = [
        { id: 1, name: 'andi' },
        { id: 2, name: 'pquest' }
      ]
      expect(storage.all).to eq records
    end
  end
end

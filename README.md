# Storage

Just another pairing interview codebase.

## Installation

Install the related dependencies:

    $ bundle install

## Usage

See the API from spec files. And see the solution from `the-solution` branch. 

We do test-drive development, the interviewer as driver, and the candidate as the pilot. I'm assume in 1 hour interview, first session is QnA, and the second session is code pairing, you can make 30 min to QnA and 30 min to code pairing.


Run the test

    $ bundle exec rspec

## Development

// TODO